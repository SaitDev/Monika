#!/usr/bin/env luajit

local utf8 = require("lua-utf8")
local ft = require("freetype")
local gd = require("gd")

local WIDTH = 800
local HEIGHT_INTERVAL = 720
local PADDING = 20 -- px
local PADDING2 = 2 * PADDING
local LETTER_PADDING_X = 9 -- px
local LETTER_PADDING_Y = 0 -- px

local FONTS_DIR = "./fonts/"
local BACKGROUNDS_DIR = "./backgrounds/"

local LINE_HEIGHT_TEST_STRING = "iIlLtThHeEF"

local PERSON_CONFIG = {
    ["m1"] = { -- monika
        background = "poem.jpg",
        font = "m1.ttf",
        fontSize = 34,
        lineSpacing = 1.15
    },

    ["s1"] = { -- sayori
        background = "poem.jpg",
        font = "s1.ttf",
        fontSize = 34
    },

    ["n1"] = { -- natsuki
        background = "poem.jpg",
        font = "n1.ttf",
        fontSize = 28
    },

    ["y1"] = { -- yuri 1 (normal)
        background = "poem.jpg",
        font = "y1.ttf",
        fontSize = 32
    },
    ["y2"] = { -- yuri 2 (fast)
        background = "poem_y1.jpg",
        font = "y2.ttf",
        fontSize = 40,
        lineHeight = 0.75
    },
    ["y3"] = { -- yuri 3 (obsessed)
        background = "poem_y2.jpg",
        font = "y3.ttf",
        fontSize = 18,
        kerning = -8,
        lineHeight = 0.75
    }
}

local DEFAULT_FREETYPE_OPTIONS = {
    disable_kerning = true
}

local ioread, ioopen, iowrite, ioflush = io.read, io.open, io.write, io.flush
local initFreeType, openFace, setCharSize, getCharIndex, getKerning =
    ft.Init_FreeType, ft.Open_Face, ft.Set_Char_Size, ft.Get_Char_Index,
    ft.Get_Kerning
local createTrueColor, alphaBlending, colorAllocate, colorAllocateAlpha, fill,
    stringFTEx, copy, filledRectangle, createFromJpeg, copyResampled, pngStr =
    gd.createTrueColor, gd.alphaBlending, gd.colorAllocate,
    gd.colorAllocateAlpha, gd.fill, gd.stringFTEx, gd.copy, gd.filledRectangle,
    gd.createFromJpeg, gd.copyResampled, gd.pngStr
local u8next, u8char, u8match = utf8.next, utf8.char, utf8.match

local function main(person)
    if not PERSON_CONFIG[person] then
        error("Unknown person "..tostring(person))
    end

    local poem_contents = ioread("*a")

    local cfg = PERSON_CONFIG[person]
    local fontFile = FONTS_DIR..cfg.font
    local fontStream = assert(ioopen(fontFile, "rb"))
    local backgroundFile = BACKGROUNDS_DIR..cfg.background

    local fontSize = cfg.fontSize
    local freeTypeOptions = cfg.extra or DEFAULT_FREETYPE_OPTIONS
    local kerning = cfg.kerning or 0

    local library = initFreeType()
    local face = openFace(library, {stream = fontStream}, 0)

    setCharSize(face, fontSize * 64, 0, 72, 0)

    local temp = createTrueColor(WIDTH, HEIGHT_INTERVAL)
    alphaBlending(temp, false)
    local tempBkg = colorAllocateAlpha(temp,255,255,255,127)
    local tempFg = colorAllocate(temp, 0, 0, 0)
    fill(temp, 0, 0, tempBkg)

    local height = HEIGHT_INTERVAL
    local image = createTrueColor(WIDTH, height)
    alphaBlending(image, true)
    local background = colorAllocateAlpha(image,255,255,255,127)
    local foreground = colorAllocate(image, 0,0,0)
    fill(image, 0, 0, background)

    local lineHeight, renderLineHeight do
        local llx, lly, _, _, urx, ury = stringFTEx(nil, foreground,
            fontFile, fontSize, 0, 50, 50, LINE_HEIGHT_TEST_STRING,
            freeTypeOptions)

        lineHeight = lly - ury
        renderLineHeight = lineHeight * (cfg.lineHeight or 1.15)
    end

    local wordStart, pos, codePoint = nil, u8next(poem_contents)
    local initialWordWidth = 3
    local wordWidth = initialWordWidth
    local lx, line = PADDING, 0
    while pos do
        local char = u8char(codePoint)
        local hasGlyph = getCharIndex(face, codePoint)
        local isBreak = u8match(char, "[%s.,;:!?]") or
            u8next(poem_contents, pos) == nil

        if hasGlyph then
            local kerningY, kerningX = 0
            local _, nextCodePoint = u8next(poem_contents, pos)
            if nextCodePoint then
                local leftIndex = getCharIndex(face, codePoint)
                local rightIndex = getCharIndex(face, nextCodePoint)
                if leftIndex and rightIndex then
                    local ftKerning = getKerning(face, leftIndex,
                        rightIndex, "DEFAULT")
                    kerningX = ftKerning.x / 64
                    kerningY = ftKerning.y / 64
                end
            end
            if not kerningX then
                kerningX = 0
            end
            kerningX = kerningX + kerning

            local _, _, lrx, lry, urx, ury = stringFTEx(temp, tempFg,
                fontFile, fontSize, 0, wordWidth + LETTER_PADDING_X,
                lineHeight + kerningY, char, freeTypeOptions)

            wordWidth = lrx + kerningX - LETTER_PADDING_X
        end

        if isBreak or not hasGlyph then
            wordStart = pos

            if (lx + wordWidth) >= (WIDTH - PADDING2) then
                lx = PADDING
                line = line + 1
            end
            if (line * renderLineHeight) >= (height - PADDING2) then
                local _image = image
                image = createTrueColor(WIDTH, height + HEIGHT_INTERVAL)
                alphaBlending(image, true)
                local background = colorAllocateAlpha(image,255,255,255,127)
                local foreground = colorAllocate(image,0,0,0)
                fill(image, 0, 0, background)

                copy(image, _image,
                    0, 0,
                    0, 0,
                    WIDTH, height)
                height = height + HEIGHT_INTERVAL
            end

            copy(image, temp,
                lx, line * renderLineHeight,
                0, 0,
                WIDTH, HEIGHT_INTERVAL)
            filledRectangle(temp, 0, 0, WIDTH, HEIGHT_INTERVAL, tempBkg)
            lx = lx + wordWidth

            wordWidth = initialWordWidth
        end

        if codePoint == 10 then
            line = line + 1
            lx = PADDING
        end

        pos, codePoint = u8next(poem_contents, pos)
    end

    local text = image

    image = createTrueColor(WIDTH, 4 * PADDING + line * renderLineHeight)
    alphaBlending(image, true)
    local backgroundImage = createFromJpeg(BACKGROUNDS_DIR..cfg.background)
    for i = 0, height / HEIGHT_INTERVAL do
        if i % 2 == 0 then
            copy(image, backgroundImage,
                0, i * HEIGHT_INTERVAL,
                0, 0,
                WIDTH, HEIGHT_INTERVAL)
        else
            copyResampled(image, backgroundImage,
                0, i * HEIGHT_INTERVAL,
                0, HEIGHT_INTERVAL - 1,
                WIDTH, HEIGHT_INTERVAL,
                WIDTH, -HEIGHT_INTERVAL)
        end
    end
    copy(image, text,
        0, PADDING,
        0, 0,
        WIDTH, height)

    if image then
        iowrite(pngStr(image))
        ioflush()
    end
end

main(...)