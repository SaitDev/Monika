using Discord;

namespace Monika.Options
{
    class MonikaOptions
    {
        public string Token { get; set; }
        public TokenType TokenType { get; set; }
    }
}